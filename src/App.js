import './App.css'
import React, { Component } from 'react'
import Header from './component/Header'
import Gallery from './component/Gallery'
import { access_key } from './config'
import axios from 'axios'
import Loading from './component/Loading'
export class App extends Component {
  state = {
    search: '',
    results: [],
    loading: true,
  }

  componentDidMount() {
    this.fetchImage('trending')
  }

  searchResult = (res) => {
    this.setState({ search: res, loading: true })
    this.fetchImage(res)
  }

  fetchImage = async (value) => {
    try {
      const { data } = await axios.get(
        `https://api.unsplash.com/search/photos/?client_id=${access_key}&query=${value}&per_page=30`
      )
      this.setState({ results: data.results, loading: false })
    } catch (error) {
      console.log(error)
    }
  }
  render() {
    return (
      <div className='App'>
        <Header searchResult={this.searchResult} />
        {this.state.loading ? (
          <Loading />
        ) : (
          <Gallery results={this.state.results} />
        )}
      </div>
    )
  }
}

export default App
