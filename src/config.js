const { REACT_APP_ACCESS_KEY } = process.env

module.exports = {
  access_key: REACT_APP_ACCESS_KEY,
}
