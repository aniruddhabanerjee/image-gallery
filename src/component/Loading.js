import React, { Component } from 'react'

export class Loading extends Component {
  render() {
    return (
      <div className='loading-screen'>
        <div className='spinner'>
          <div></div>
          <div></div>
        </div>
      </div>
    )
  }
}

export default Loading
