import React, { Component } from 'react'

export class Header extends Component {
  state = {
    search: '',
  }
  handleSearch = (e) => {
    this.setState({ search: e.target.value })
  }
  onSubmitHandler = (e) => {
    e.preventDefault()
    this.props.searchResult(this.state.search)
  }
  render() {
    return (
      <div className='header'>
        <h1>Image Gallery</h1>
        <form onSubmit={this.onSubmitHandler}>
          <input
            onChange={this.handleSearch}
            type='text'
            name='search'
            placeholder='Search for Image'
            className='search-box'
            value={this.state.search}
          />
          <button name='submit' type='submit' className='btn'>
            <i className='fas fa-search'></i>
          </button>
        </form>
      </div>
    )
  }
}

export default Header
