import React, { Component } from 'react'
import Modal from 'react-modal'
export class ModalScreen extends Component {
  render() {
    const modalStyle = {
      content: {
        display: 'flex',
        justifyContent: 'center',
        border: 'none',
        padding: 'none',
        overflow: 'none',
        background: '#0f0e1c',
      },
    }

    Modal.setAppElement(document.getElementById('root'))
    return (
      <Modal
        contentLabel='Image preview'
        style={modalStyle}
        isOpen={this.props.modalIsOpen}
        onRequestClose={this.onCloseModal}
      >
        <div className='image-container'>
          <img
            className='img-preview'
            src={this.props.currentImg}
            alt='modal view'
          />
          <button
            name='close-btn'
            onClick={this.props.handleCloseButton}
            className='close-btn'
          >
            <i className='fas fa-times'></i>
          </button>
        </div>
      </Modal>
    )
  }
}

export default ModalScreen
