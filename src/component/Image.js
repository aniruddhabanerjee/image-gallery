import React, { Component } from 'react'

export class Image extends Component {
  render() {
    return (
      <img
        onClick={() => this.props.handleClickImage(this.props.url.regular)}
        className='img'
        src={this.props.src}
        alt={this.props.alt}
      />
    )
  }
}

export default Image
