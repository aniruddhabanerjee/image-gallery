import React, { Component } from 'react'
import Image from './Image'
import ModalScreen from './ModalScreen'
export class Gallery extends Component {
  state = {
    currentImg: null,
    modalIsOpen: false,
  }
  handleCloseButton = () => {
    this.setState({
      modalIsOpen: false,
    })
  }
  handleClickImage = (url) => {
    this.setState({
      modalIsOpen: true,
      currentImg: url,
    })
  }
  onCloseModal = () => {
    this.setState({
      modalIsOpen: false,
    })
  }

  render() {
    return (
      <div className='gallery'>
        <ModalScreen
          modalIsOpen={this.state.modalIsOpen}
          onCloseModal={this.onCloseModal}
          currentImg={this.state.currentImg}
          handleCloseButton={this.handleCloseButton}
        />
        {this.props.results.length === 0 ? (
          <h1>No Image Found</h1>
        ) : (
          this.props.results.map((item) => (
            <Image
              key={item.id}
              url={item.urls}
              src={item.urls.small}
              alt={item.alt_description}
              handleClickImage={this.handleClickImage}
            />
          ))
        )}
      </div>
    )
  }
}

export default Gallery
